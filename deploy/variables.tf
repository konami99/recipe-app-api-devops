variable "prefix" {
  type        = string
  default     = "raad"
  description = "description"
}

variable "project" {
  default = "recipt-app-api-devops"
}

variable "contact" {
  default = "konami99@hotmail.com"
}

variable "db_username" {

}

variable "db_password" {

}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  default = "011079945432.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  default = "011079945432.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {

}

variable "dns_zone_name" {
  default = "richardchou.id.au"
}

variable "subdomain" {
  type = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}